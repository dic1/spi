/**
 * API for SPI bus, where the ATMega644xx acts as the master.
 */

#ifndef SPI_H
#define SPI_H

#include "HtlStddef.h"
#include "RingBuffer.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <math.h>

/**
 * Function: SPI_Init
 *
 * Description:
 *
 * Parameters:
 *	bitrate - The desired bitrate of the serial clock.
 *
 *	clockFrequency - The frequency of your processor.
 *
 *	bufferSize - The size of the internal read buffer.
 *
 *	setDataOrderLSBFirst - If ETRUE the LSB of the data word is transmitted
 *	first, If EFALSE the MSB of the data word is transmitted first.
 *
 *	setCpol - If ETRUE then the serial clock is high when idle and the
 *	falling edge is the leading edge and the rising edge is the trailing
 *	edge. If EFALSE then the serial clock is low when idle and the
 *	rising edge is the leading edge and the falling edge is the trailing
 *	edge.
 *
 *	setCpha - The settings of the Clock Phase bit (CPHA) determine if data
 *	is sampled on the leading (first) or trailing (last) edge of SCK.
 *	If ETRUE sample data on the trailing edge and setup on the leading edge.
 *	If EFALSE sample data on the leading edge and setup on the trailing
 *	edge.
 *
 * Returns:
 *	ETRUE if the SPI interface successfully initialized, else EFALSE.
 */
TBool SPI_Init(unsigned long bitrate,
               unsigned long clockFrequency,
               unsigned char bufferSize,
               TBool setDataOrderLSBFirst,
               TBool setCpol,
               TBool setCpha);

void SPI_Destroy();

void SPI_Transmit(unsigned char* buffer, unsigned char size);

void SPI_Receive(unsigned char* buffer, unsigned char size);

#endif  // SPI_H
