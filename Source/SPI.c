#include "SPI.h"

#define LENGTH_OF(x) (sizeof(x) / sizeof(x[0]))

static const unsigned char SPI_PrescalerValues[] = {2, 4, 8, 16, 32, 64, 128};

static TRingBuffer readBuffer;

/******************** Private Function Declarations ********************/

/**
 * Returns a mask that sets its first two bits to the value for SPR00 and SPR01.
 */
static unsigned char SPI_CalculatePrescaler(unsigned long bitrate,
                                            unsigned long clockFrequency,
                                            TBool* needsDoubleSpeed);

/******************** Public Function Definitions ********************/

TBool SPI_Init(unsigned long bitrate,
               unsigned long clockFrequency,
               unsigned char bufferSize,
               TBool setDataOrderLSBFirst,
               TBool setCpol,
               TBool setCpha) {
  readBuffer = RingBufferCreate(bufferSize);

  if (NULL == readBuffer) {
    SPI_Destroy();
    return EFALSE;
  }

  unsigned char controlRegister = 0;
  TBool needsDoubleSpeed;

  // Set prescaler.
  controlRegister |=
      SPI_CalculatePrescaler(bitrate, clockFrequency, &needsDoubleSpeed);

  // Enable interrupts for SPI.
  controlRegister |= (1 << SPIE);

  // Required for any SPI operations.
  controlRegister |= (1 << SPE);

  if (setDataOrderLSBFirst)
    controlRegister |= (1 << DORD);

  // Select Master SPI mode
  controlRegister |= (1 << MSTR);

  if (setCpol)
    controlRegister |= (1 << CPOL);

  if (setCpha)
    controlRegister |= (1 << CPHA);

  if (needsDoubleSpeed)
    SPSR |= (1 << SPI2X);

  // Set SS, MOSI and SCK to output.
  DDRB |= (1 << PINB4) | (1 << PINB5) | (1 << PINB7);

  SPCR |= controlRegister;

  return ETRUE;
}

void SPI_Destroy() {
  cli();

  if (NULL != readBuffer)
    RingBufferDestroy(readBuffer);

  sei();
}

void SPI_Transmit(unsigned char* buffer, unsigned char size) {
  unsigned char i;

  // After setting SPDR an interrupt is requested
  for (i = 0; i < size; i++)
    SPDR = buffer[i];
}

void SPI_Receive(unsigned char* buffer, unsigned char size) {
  unsigned char i;

  for (i = 0; i < size; i++)
    RingBufferRead(readBuffer, &buffer[i]);
}

/******************** Private Function Definitions ********************/

static unsigned char SPI_CalculatePrescaler(unsigned long bitrate,
                                            unsigned long clockFrequency,
                                            TBool* needsDoubleSpeed) {
  unsigned char prescalerIndex = 0;
  unsigned char bestPrescalerIndex = 0;

  double bestDelta =
      bitrate - ((double)clockFrequency / SPI_PrescalerValues[prescalerIndex]);
  double delta;

  for (prescalerIndex = 1; prescalerIndex < LENGTH_OF(SPI_PrescalerValues);
       prescalerIndex++) {
    delta = bitrate -
            ((double)clockFrequency / SPI_PrescalerValues[prescalerIndex]);

    if (fabs(bestDelta) > fabs(delta)) {
      bestDelta = delta;
      bestPrescalerIndex = prescalerIndex;
    }
  }

  unsigned char prescaler = 0;

  switch (bestPrescalerIndex) {
    // 2
    case 0:
      *needsDoubleSpeed = ETRUE;
      break;

    // 4
    case 1:
      *needsDoubleSpeed = EFALSE;
      break;

    // 8
    case 2:
      *needsDoubleSpeed = ETRUE;
      prescaler |= (1 << SPR0);
      break;

    // 16
    case 3:
      *needsDoubleSpeed = ETRUE;
      prescaler |= (1 << SPR0);
      break;

    // 32
    case 4:
      *needsDoubleSpeed = ETRUE;
      prescaler |= (1 << SPR1);
      break;

    // 64
    case 5:
      *needsDoubleSpeed = ETRUE;
      prescaler |= (1 << SPR0) | (1 << SPR1);
      break;

    // 128
    case 6:
      *needsDoubleSpeed = EFALSE;
      prescaler |= (1 << SPR0) | (1 << SPR1);
      break;
  }

  return prescaler;
}

/******************** Handling Interrupts ********************/

// Transfer complete interrupt
ISR(SPI_STC_vect) {
  // The received data in stored in SPDR.
  RingBufferWrite(readBuffer, SPDR);

  // The interrupt flag SPIF in SPSR is cleared by the hardware.
}
