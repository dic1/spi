/*
 * SPI.c
 *
 * Created: 21/05/2021 10:47:25
 */

#include "SPI.h"
#include <avr/io.h>

int main(void) {
  SPI_Init(10000UL, F_CPU, EFALSE, EFALSE, EFALSE);

  unsigned char data[5] = {5, 10, 75, 98, 255};

  SPI_Transmit(data, 5);

  SPI_Destroy();
}

